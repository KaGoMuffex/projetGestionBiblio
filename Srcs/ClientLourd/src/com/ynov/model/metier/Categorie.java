package com.ynov.model.metier;

public class Categorie {
	
	private Long id;
	private String nom;
	private String decription;
	
	public Categorie() {
	}

	public Categorie(Long id, String nom, String decription) {
		super();
		this.id = id;
		this.nom = nom;
		this.decription = decription;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDecription() {
		return decription;
	}

	public void setDecription(String decription) {
		this.decription = decription;
	}

}
