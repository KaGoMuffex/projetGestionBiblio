package com.ynov.model.metier;

import java.util.Date;

public abstract class Personne {

	private Long id;
	private String nom;
	private String prenom;
	private String sexe;
	private String urlPhoto;
	private String email;
	private Date dateDeNaissance;
	
	public Personne(Long id, String nom, String prenom, String sexe, String urlPhoto, String email,
			Date dateDeNaissance) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.sexe = sexe;
		this.urlPhoto = urlPhoto;
		this.email = email;
		this.dateDeNaissance = dateDeNaissance;
	}

	public Personne() {
		// TODO Auto-generated constructor stub
	}

	public long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getNom() {
		return nom;
	}
	
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public String getPrenom() {
		return prenom;
	}
	
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	public String getSexe() {
		return sexe;
	}
	
	public void setSexe(String sexe) {
		this.sexe = sexe;
	}
	
	public String getUrlPhoto() {
		return urlPhoto;
	}
	
	public void setUrlPhoto(String urlPhoto) {
		this.urlPhoto = urlPhoto;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public Date getDateDeNaissance() {
		return dateDeNaissance;
	}
	
	public void setDateDeNaissance(Date dateDeNaissance) {
		this.dateDeNaissance = dateDeNaissance;
	}
}
