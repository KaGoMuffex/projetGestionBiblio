package com.ynov.model.metier;

import java.util.Date;

public class Pret {
	
	private Long id;
	private Livre livre;
	private Utilisateur utilisateur;
	private Date dateDePret;
	private int duree;
	
	public Pret() {
		
	}

	public Pret(Long id, Livre livre, Utilisateur utilisateur, Date dateDePret, int duree) {
		super();
		this.id = id;
		this.livre = livre;
		this.utilisateur = utilisateur;
		this.dateDePret = dateDePret;
		this.duree = duree;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Livre getLivre() {
		return livre;
	}

	public void setLivre(Livre livre) {
		this.livre = livre;
	}

	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

	public Date getDateDePret() {
		return dateDePret;
	}

	public void setDateDePret(Date dateDePret) {
		this.dateDePret = dateDePret;
	}

	public int getDuree() {
		return duree;
	}

	public void setDuree(int duree) {
		this.duree = duree;
	}

}
