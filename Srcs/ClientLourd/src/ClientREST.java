import java.io.IOException;
import java.net.URI;
import java.util.List;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.UriBuilder;
import org.codehaus.jackson.map.ObjectMapper;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import com.ynov.model.metier.Livre;

public class ClientREST {

	public static void main(String[] args) {
        //on utilise une fabrique pour cr�er le client
        Client client = Client.create(new DefaultClientConfig());
        
        //on cr� URI pour pouvoir interagir en utilisant l�objet client en http
        URI uri=UriBuilder.fromUri("http://localhost:8080/Bibliotheque/").build();
        
        
        
        
        /// Connexion

        MultivaluedMap<String, String> formData = new MultivaluedMapImpl();
        formData.add("pseudo", "toto");
        formData.add("mdp", "azerty");
        
        ClientResponse responseConnexion = client.resource(uri)
                                    .path("api")
                                    .path("connexion")
                                    .header("Content-Type", "application/x-www-form-urlencoded")
                                    .put(ClientResponse.class, formData);
        
       
        System.out.println("Conexion : "+responseConnexion.getStatus());
        
        List<NewCookie> cookies = responseConnexion.getCookies();
        
        
        
        
        
        
        
        //consulter tous les livres
        WebResource ressource = client.resource(uri)
                                    .path("api")
                                    .path("livres");
        
        for (NewCookie newCookie : cookies) {
			ressource.cookie(newCookie);
		}      
                             
        ClientResponse response = ressource.get(ClientResponse.class);
        
        //on recup�re la reponse au format json
        String corpsRepHttp = response.getEntity(String.class);
        
        
        cookies = response.getCookies();
        
        //on va utiliser jackson et un ObjectMapper
        ObjectMapper mapper = new ObjectMapper();
        
        Livre[] livres;
		try {
			livres = mapper.readValue(corpsRepHttp, Livre[].class);

	        afficherLivres(livres);
	        
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
		
		
        
        
        //Faire un emprunt
        ressource = client.resource(uri)
                .path("api")
                .path("emprunter")
                .path("101"); //idLivre;
        
        for (NewCookie newCookie : cookies) {
			ressource.cookie(newCookie);
		}      
               
        ClientResponse responseEmprunt = ressource.put(ClientResponse.class);
        
        System.out.println("Emprunt : "+responseEmprunt.getStatus());
        

        cookies = responseEmprunt.getCookies();

        
        
        
		
		
		
        //Consulter un livre
        ressource = client.resource(uri)
                .path("api")
                .path("livre")
                .path("101"); //idLivre;

		for (NewCookie newCookie : cookies) {
			ressource.cookie(newCookie);
		}  
        ClientResponse response2 = ressource.get(ClientResponse.class);
        
        String corpsRepHttp2 = response2.getEntity(String.class);
        
        cookies = response2.getCookies();
        
        Livre livre;
		try {
			livre = mapper.readValue(corpsRepHttp2, Livre.class);
			afficherLivre(livre);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        
        
		
        
        
        //Deconnexion
        ressource = client.resource(uri)
                .path("api")
                .path("deconnexion");

		for (NewCookie newCookie : cookies) {
			ressource.cookie(newCookie);
		}  
        ClientResponse responseDeconnexion = ressource.get(ClientResponse.class);
        
        System.out.println("Deconexion : "+responseDeconnexion.getStatus());
        
    }
	
	public static void afficherLivre(Livre livre) {
        System.out.println("Livre:\n\tid:\t"+livre.getId()+
        		"\n\ttitre:\t"+livre.getTitre()+
        		"\n\tresume:\t"+livre.getResume()+
        		"\n\tquantite:\t"+livre.getQuantite()+
        		"\n\tisbn:\t"+livre.getIsbn()+
        		"\n\tauteur:\t"+livre.getAuteur().getNom()+ " "+ livre.getAuteur().getPrenom() +
        		"\n\tcategorie:\t"+livre.getCategorie().getNom());
	}
	
	public static void afficherLivres(Livre[] livres) {
		for(Livre l:livres){
			afficherLivre(l);
		}
	}
	
}