<%@page import="com.ynov.controleur.Manager"%>
<%@page import="com.ynov.service.CategoriePOJO"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>
    <!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title>Liste des catégories</title>
  
  
  
      <link rel="stylesheet" href="css/ConsulterLivre.css">
  <link rel="stylesheet" href="css/footer.css" />

  
</head>

<body>


	<div><a href="./index.jsp"><h1>Accueil</h1></a></div>
	
  <div class="filters">

  <!--<input type="text" id="id" placeholder="Id" />-->
  <input type="text" id="nom" placeholder="nom" />
  <button>Reset</button>
 
</div>
<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Nom</th>
      <th>Description</th>
      <th>Modification</th>
    </tr>
  </thead>
  <tbody>
  <%
  List<CategoriePOJO> categories = Manager.getInstance().consultationCategories();
  for(CategoriePOJO categorie : categories){
	  out.println("<tr data-nom=\""+categorie.getNom()+"\">");
		  out.println("<td>" + categorie.getId() +"</td>");
		  out.println("<td>" + categorie.getNom() +"</td>");
		  out.println("<td>" + categorie.getDecription() +"</td>");
		  
	  out.println("</tr>");
  }
  %>
  </tbody>
</table>


    <a href="ConsulterLivre.jsp"> <button>Consulter les<br/> livres</button></a>
    <a href="ConsulterAuteur.jsp"> <button>Consulter les auteurs</button></a>


  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery-throttle-debounce/1.1/jquery.ba-throttle-debounce.min.js'></script>
  <script  src="js/Filtre.js"></script>



<footer>
      <div>
        <div id="txtfooter">
            Alexandre Boucher - Adrien Cazzola - Vincent Dubois - Juliette Mai - Thomas Palazetti
          </br>
            Projet JEE
        </div>
      </div>
    </footer>
</body>

</html>
