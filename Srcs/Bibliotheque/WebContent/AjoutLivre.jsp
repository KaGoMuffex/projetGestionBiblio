<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>
    <!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title>Inscription</title>
    <link rel="stylesheet" href="css/inscription.css" />
	<link rel="stylesheet" href="css/footer.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js"></script>

</head>

<body>

	<div><a href="./index.jsp"><h1>Accueil</h1></a></div>
	
	
  <div class="form-wrap">
		<div class="tabs">
			<h3 class="signup-tab"><a class="active" href="#signup-tab-content">Ajouter un livre</a></h3>
		</div><!--.tabs-->

		<div class="tabs-content">
			<div id="signup-tab-content" class="active">
				<form class="signup-form" action="" method="post">
			          <input type="text" class="input" id="titre" autocomplete="off" placeholder="Titre">
			          <input type="text" class="input" id="auteur" autocomplete="off" placeholder="Auteur">
			          <input type="text" class="input" id="isbn" autocomplete="off" placeholder="ISBN">
			          <input type="text" class="input" id="annee" autocomplete="off" placeholder="AnnÃ©e">
					  <input type="text" class="input" id="edition" autocomplete="off" placeholder="Ãdition">
					  <input type="submit" class="button" value="Ajout"> <a type="button" href="GestionLivre.html">Annuler</a>
				</form><!--.login-form-->
			</div><!--.signup-tab-content-->
	</div><!--.form-wrap-->
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

  

    	<script  src="js/inscription.js"></script>


<footer>
			<div>
				<div id="txtfooter">
						Alexandre Boucher - Adrien Cazzola - Vincent Dubois - Juliette Mai - Thomas Palazetti
					</br>
						Projet JEE
				</div>
			</div>
		</footer>

</body>

</html>
