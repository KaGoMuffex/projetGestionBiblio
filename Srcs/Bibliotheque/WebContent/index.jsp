<%@page import="com.ynov.utilitaire.Role"%>
<%@page import="com.ynov.service.UtilisateurPOJO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Accueil</title>
	<link rel="stylesheet" href="css/style.css" />
	<link rel="stylesheet" href="css/footer.css" />
</head>
<%
if(request.getParameter("deconnexion") != null){
	session.removeAttribute("user");
}
%>
<body>
	<div id="content">
		<header>
			<h1>Bienvenue à la bibliothèque</h1></div>
		</header>
		<div id="pnlButton">
		<%
			if(session.getAttribute("user") != null){
				  out.println("<a href=\"?deconnexion\">Deconnexion</a>");
				  if( ((UtilisateurPOJO)session.getAttribute("user")).getRole() != Role.ADMIN ){
					 // out.println("<a href=\"ConnexionMembre.jsp\">Espace Membre</a>");
				  }else{
					  out.println("<a href=\"GestionLivre.jsp\">Gerer les livres</a>");
				  }
				  out.println("<a href=\"ConsulterLivre.jsp\">Rechercher & Consulter</a>");
			    
			}else{
				  out.println("<a href=\"Inscription.jsp\">Inscription/Connexion</a>");
			}
		%>
		</div>

		<footer>
			<div>
				<div id="txtfooter">
						Alexandre Boucher - Adrien Cazzola - Vincent Dubois - Juliette Mai - Thomas Palazetti
					<br/>
						Projet JEE
				</div>
			</div>
		</footer>
	</div>
</body>