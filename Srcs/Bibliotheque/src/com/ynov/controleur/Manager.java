package com.ynov.controleur;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.servlet.UnavailableException;

import com.sun.jersey.api.NotFoundException;
import com.ynov.model.technique.AuteurManager;
import com.ynov.model.technique.CategorieManager;
import com.ynov.model.technique.DuplicatedEmailException;
import com.ynov.model.technique.DuplicatedPseudoException;
import com.ynov.model.technique.ForbiddenException;
import com.ynov.model.technique.LivreManager;
import com.ynov.model.technique.PretManager;
import com.ynov.model.technique.UtilisateurManager;
import com.ynov.service.AuteurPOJO;
import com.ynov.service.CategoriePOJO;
import com.ynov.service.LivrePOJO;
import com.ynov.service.PretPOJO;
import com.ynov.service.UtilisateurPOJO;
import com.ynov.stockage.DaoFactory;
import com.ynov.utilitaire.Role;

public final class Manager {
	
	private static Manager instance = null;
	private Vector<AuteurManager> auteurs;
	private Vector<CategorieManager> categories;
	private Vector<LivreManager> livres;
	private Vector<PretManager> prets;
	private Vector<UtilisateurManager> utilisateurs;
	private UtilisateurManager utilisateurConnnecte = null;

	private Manager() {
		auteurs = new Vector<AuteurManager>();
		categories = new Vector<CategorieManager>();
		livres = new Vector<LivreManager>() ;
		prets = new Vector<PretManager>() ;
		utilisateurs = new Vector<UtilisateurManager>() ;
		init();
	}
	
	public static Manager getInstance() {
		if(instance == null) {
			instance = new Manager();
		}
		return instance;
	}
	
	/// INIT
	
	private void init() {
		initAuteur();
		initCategorie();
		initLivre();
		initPret();
		initUtilisateur();
	}
	private void initAuteur() {
		auteurs.clear();
		List<AuteurPOJO> auteurPOJOs = null;
		DaoFactory dao = DaoFactory.getInstance();
		auteurPOJOs = dao.getDaoAuteur().lireTous();
		for (AuteurPOJO auteurPOJO : auteurPOJOs) {
			auteurs.add(new AuteurManager(auteurPOJO, dao.getDaoAuteur()));
		}
	}
	private void initCategorie() {
		categories.clear();
		List<CategoriePOJO> categoriePOJOs = null;
		DaoFactory dao = DaoFactory.getInstance();
		categoriePOJOs = dao.getDaoCategorie().lireTous();
		for (CategoriePOJO categoriePOJO : categoriePOJOs) {
			categories.add(new CategorieManager(categoriePOJO, dao.getDaoCategorie()));
		}
	}
	private void initLivre() {
		livres.clear();
		List<LivrePOJO> livrePOJOs = null;
		DaoFactory dao = DaoFactory.getInstance();
		livrePOJOs = dao.getDaoLivre().lireTous();
		for (LivrePOJO livrePOJO : livrePOJOs) {
			livres.add(new LivreManager(livrePOJO, dao.getDaoLivre()));
		}
	}
	private void initPret() {
		prets.clear();
		List<PretPOJO> pretPOJOs = null;
		DaoFactory dao = DaoFactory.getInstance();
		pretPOJOs = dao.getDaoPret().lireTous();
		for (PretPOJO pretPOJO : pretPOJOs) {
			prets.add(new PretManager(pretPOJO, dao.getDaoPret()));
		}
	}
	private void initUtilisateur() {
		utilisateurs.clear();
		List<UtilisateurPOJO> utilisateurPOJOs = null;
		DaoFactory dao = DaoFactory.getInstance();
		utilisateurPOJOs = dao.getDaoUtilisateur().lireTous();
		for (UtilisateurPOJO utilisateurPOJO : utilisateurPOJOs) {
			utilisateurs.add(new UtilisateurManager(utilisateurPOJO, dao.getDaoUtilisateur()));
		}
	}
	
	/// Utilisateur actuel

	public boolean connexion(String pseudo, String mdp, boolean encrypt) {
		
		if(encrypt) {
			//Encrypte mdp
			try {
				MessageDigest md5 = MessageDigest.getInstance("MD5");
				md5.reset();
				md5.update(mdp.getBytes());
				mdp = new String(md5.digest());
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		for (UtilisateurManager m : this.utilisateurs) {
			UtilisateurPOJO pojo = m.getPOJO();
			if(pojo.getPseudo().equals(pseudo) && pojo.getMdp().equals(mdp)) {
				this.utilisateurConnnecte = m;
				return true;
			}
		}
		this.utilisateurConnnecte = null;
		return false;
	}

	public boolean connexion(String pseudo, String mdp) {
		return connexion(pseudo, mdp, true);
	}
	
	public void deconnexion() {
		this.utilisateurConnnecte = null;
	}
	
	public UtilisateurPOJO getUtilisateurConnecte() {
		return utilisateurConnnecte.getPOJO();
	}
	
	/// LIVRE

	public List<LivrePOJO> consultationLivres() {
		List<LivrePOJO> ret = null;
		ret = new Vector<>();
		for (LivreManager m: this.livres) {
			ret.add(m.getPOJO());
		}
		return ret;
	}

	public LivrePOJO consultationLivre(Long id) {
		for (LivreManager m : this.livres) {
			if(m.getPOJO().getId() == id) {
				return m.getPOJO();
			}
		}
		throw new NotFoundException();
	}
	
	public void modifierLivre(Long id, LivrePOJO newLivre) throws ForbiddenException {
		if(this.utilisateurConnnecte == null || this.utilisateurConnnecte.getPOJO().getRole() != Role.ADMIN) {
			throw new ForbiddenException();
		}
		for (LivreManager m : this.livres) {
			if(m.getPOJO().getId() == id) {
				m.update(newLivre);
			}
		}
	}
	
	public void ajouterLivre(LivrePOJO newLivre) throws ForbiddenException {
		if(this.utilisateurConnnecte == null || this.utilisateurConnnecte.getPOJO().getRole() != Role.ADMIN) {
			throw new ForbiddenException();
		}
		DaoFactory.getInstance().getDaoLivre().inserer(newLivre);
		// r�cup�rer le nouvel �l�ment
		initLivre();
	}
	
	public LivrePOJO supprimerLivre(Long id) throws ForbiddenException {
		if(this.utilisateurConnnecte == null || this.utilisateurConnnecte.getPOJO().getRole() != Role.ADMIN) {
			throw new ForbiddenException();
		}
		for (LivreManager m : livres) {
			if(m.getPOJO().getId() == id) {
				m.supprimer();
				livres.remove(m);
				return m.getPOJO();
			}
		}
		throw new NotFoundException();
	}
	
	public void emprunterLivre(Long id) throws ForbiddenException, UnavailableException {
		if(this.utilisateurConnnecte == null) {
			throw new ForbiddenException();
		}
		LivrePOJO leLivre = null;
		for (LivreManager livreManager : livres) {
			if(livreManager.getPOJO().getId() == id && livreManager.getPOJO().getQuantite() > 0) {
				PretManager pret = new PretManager(DaoFactory.getInstance().getDaoPret());
				PretPOJO pretPojo = new PretPOJO(null, livreManager.getPOJO(), this.utilisateurConnnecte.getPOJO(), new Date(), 20);
				pret.ajouter(pretPojo);
				leLivre = livreManager.getPOJO();
				leLivre.setQuantite(leLivre.getQuantite() - 1);
				livreManager.update(leLivre);
				initPret();
			}else if(livreManager.getPOJO().getId() == id && livreManager.getPOJO().getQuantite() <= 0){
				System.out.println("id:"+livreManager.getPOJO().getId() + "  qte:"+livreManager.getPOJO().getQuantite());
				throw new UnavailableException("Livre non disponible");
			}
		}
		if(leLivre == null) {
			throw new NotFoundException();
		}
	}
	
	/// CATEGORIE

	public List<CategoriePOJO> consultationCategories() {
		List<CategoriePOJO> ret = null;
		ret = new Vector<>();
		for (CategorieManager m: this.categories) {
			ret.add(m.getPOJO());
		}
		return ret;
	}
	
	public CategoriePOJO consultationCategorie(Long id) {
		for (CategorieManager m : this.categories) {
			if(m.getPOJO().getId() == id) {
				return m.getPOJO();
			}
		}
		throw new NotFoundException();
	}
	
	public void modifierCategorie(Long id, CategoriePOJO newCategorie) throws ForbiddenException {

		if(this.utilisateurConnnecte == null || this.utilisateurConnnecte.getPOJO().getRole() != Role.ADMIN) {
			throw new ForbiddenException();
		}
		for (CategorieManager m : this.categories) {
			if(m.getPOJO().getId() == id) {
				m.update(newCategorie);
			}
		}
	}
	
	public void ajouterCategorie(CategoriePOJO newCategorie) throws ForbiddenException {

		if(this.utilisateurConnnecte == null || this.utilisateurConnnecte.getPOJO().getRole() != Role.ADMIN) {
			throw new ForbiddenException();
		}
		DaoFactory.getInstance().getDaoCategorie().inserer(newCategorie);
		// r�cup�rer le nouvel �l�ment
		initCategorie();
	}
	
	public CategoriePOJO supprimerCategorie(Long id) throws ForbiddenException {
		if(this.utilisateurConnnecte == null || this.utilisateurConnnecte.getPOJO().getRole() != Role.ADMIN) {
			throw new ForbiddenException();
		}
		for (CategorieManager m : this.categories) {
			if(m.getPOJO().getId() == id) {
				m.supprimer();
				this.categories.remove(m);
				return m.getPOJO();
			}
		}
		throw new NotFoundException();
	}
	
	/// AUTEUR

	public List<AuteurPOJO> consultationAuteurs() {
		List<AuteurPOJO> ret = null;
		ret = new Vector<>();
		for (AuteurManager m: this.auteurs) {
			ret.add(m.getPOJO());
		}
		return ret;
	}
	
	public AuteurPOJO consultationAuteur(Long id) {
		for (AuteurManager m : this.auteurs) {
			if(m.getPOJO().getId() == id) {
				return m.getPOJO();
			}
		}
		throw new NotFoundException();
	}
	
	public void modifierAuteur(Long id, AuteurPOJO newAuteur) throws ForbiddenException {
		if(this.utilisateurConnnecte == null || this.utilisateurConnnecte.getPOJO().getRole() != Role.ADMIN) {
			throw new ForbiddenException();
		}
		for (AuteurManager m : this.auteurs) {
			if(m.getPOJO().getId() == id) {
				m.update(newAuteur);
			}
		}
	}
	
	public void ajouterAuteur(AuteurPOJO newAuteur) throws ForbiddenException {
		if(this.utilisateurConnnecte == null || this.utilisateurConnnecte.getPOJO().getRole() != Role.ADMIN) {
			throw new ForbiddenException();
		}
		DaoFactory.getInstance().getDaoAuteur().inserer(newAuteur);
		// r�cup�rer le nouvel �l�ment
		initAuteur();
	}
	
	public AuteurPOJO supprimerAuteur(Long id) throws ForbiddenException {
		if(this.utilisateurConnnecte == null || this.utilisateurConnnecte.getPOJO().getRole() != Role.ADMIN) {
			throw new ForbiddenException();
		}
		for (AuteurManager m : this.auteurs) {
			if(m.getPOJO().getId() == id) {
				m.supprimer();
				this.auteurs.remove(m);
				return m.getPOJO();
			}
		}
		throw new NotFoundException();
	}
	
	/// UTILISATEUR

	public List<UtilisateurPOJO> consultationUtilisateurs() throws ForbiddenException {
		if(this.utilisateurConnnecte == null || this.utilisateurConnnecte.getPOJO().getRole() != Role.ADMIN) {
			throw new ForbiddenException();
		}
		List<UtilisateurPOJO> ret = new Vector<>();
		for (UtilisateurManager m: this.utilisateurs) {
			ret.add(m.getPOJO());
		}
		return ret;
	}
	
	public UtilisateurPOJO consultationUtilisateur(Long id) throws ForbiddenException {
		if(this.utilisateurConnnecte == null || this.utilisateurConnnecte.getPOJO().getRole() != Role.ADMIN) {
			throw new ForbiddenException();
		}
		for (UtilisateurManager m : this.utilisateurs) {
			if(m.getPOJO().getId() == id) {
				return m.getPOJO();
			}
		}
		throw new NotFoundException();
	}
	
	public void modifierUtilisateur(Long id, UtilisateurPOJO newUtilisateur) throws ForbiddenException {
		if(this.utilisateurConnnecte == null || this.utilisateurConnnecte.getPOJO().getRole() != Role.ADMIN) {
			throw new ForbiddenException();
		}
		//Encrypte mdp
		try {
			MessageDigest encrypt = MessageDigest.getInstance("MD5");
			encrypt.reset();
			encrypt.update(newUtilisateur.getMdp().getBytes());
			newUtilisateur.setMdp(new String(encrypt.digest()));
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (UtilisateurManager m : this.utilisateurs) {
			if(m.getPOJO().getId() == id) {
				m.update(newUtilisateur);
			}
		}
	}
	
	public void ajouterUtilisateur(UtilisateurPOJO newUtilisateur) throws DuplicatedEmailException, DuplicatedPseudoException {
		if(this.utilisateurConnnecte != null && this.utilisateurConnnecte.getPOJO().getRole() != Role.ADMIN && newUtilisateur.getRole() == Role.ADMIN) {
			newUtilisateur.setRole(Role.USER);
		}
		for (UtilisateurManager m : this.utilisateurs) {
			if(m.getPOJO().getEmail().equals(newUtilisateur.getEmail())) {
				throw new DuplicatedEmailException();
			}else if(m.getPOJO().getPseudo().equals(newUtilisateur.getPseudo())) {
				throw new DuplicatedPseudoException();
			}
		}
		
		//Encrypte mdp
		try {
			MessageDigest encrypt = MessageDigest.getInstance("MD5");
			encrypt.reset();
			encrypt.update(newUtilisateur.getMdp().getBytes());
			newUtilisateur.setMdp(new String(encrypt.digest()));
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DaoFactory.getInstance().getDaoUtilisateur().inserer(newUtilisateur);
		// r�cup�rer le nouvel �l�ment
		initUtilisateur();
	}
	
	public UtilisateurPOJO supprimerUtilisateur(Long id) throws ForbiddenException {
		if(this.utilisateurConnnecte == null || this.utilisateurConnnecte.getPOJO().getRole() != Role.ADMIN) {
			throw new ForbiddenException();
		}
		for (UtilisateurManager m : this.utilisateurs) {
			if(m.getPOJO().getId() == id) {
				m.supprimer();
				this.utilisateurs.remove(m);
				return m.getPOJO();
			}
		}
		throw new NotFoundException();
	}

	/// PRET

	public List<PretPOJO> consultationPrets() throws ForbiddenException {
		if(this.utilisateurConnnecte == null || this.utilisateurConnnecte.getPOJO().getRole() != Role.ADMIN) {
			throw new ForbiddenException();
		}
		List<PretPOJO> ret = null;
		ret = new Vector<>();
		for (PretManager m: this.prets) {
			ret.add(m.getPOJO());
		}
		return ret;
	}
	
	public PretPOJO consultationPret(Long id) throws ForbiddenException {
		if(this.utilisateurConnnecte == null || this.utilisateurConnnecte.getPOJO().getRole() != Role.ADMIN) {
			throw new ForbiddenException();
		}
		for (PretManager m : this.prets) {
			if(m.getPOJO().getId() == id) {
				return m.getPOJO();
			}
		}
		throw new NotFoundException();
	}
	
	public void modifierPret(Long id, PretPOJO newPret) throws ForbiddenException {
		if(this.utilisateurConnnecte == null || this.utilisateurConnnecte.getPOJO().getRole() != Role.ADMIN) {
			throw new ForbiddenException();
		}
		for (PretManager m : this.prets) {
			if(m.getPOJO().getId() == id) {
				m.update(newPret);
			}
		}
	}
	
	public void ajouterPret(PretPOJO newPret) throws ForbiddenException {
		if(this.utilisateurConnnecte == null || this.utilisateurConnnecte.getPOJO().getRole() != Role.ADMIN) {
			throw new ForbiddenException();
		}
		DaoFactory.getInstance().getDaoPret().inserer(newPret);
		// r�cup�rer le nouvel �l�ment
		initPret();
	}
	
	public PretPOJO supprimerPret(Long id) throws ForbiddenException {
		if(this.utilisateurConnnecte == null || this.utilisateurConnnecte.getPOJO().getRole() != Role.ADMIN) {
			throw new ForbiddenException();
		}
		for (PretManager m : this.prets) {
			if(m.getPOJO().getId() == id) {
				m.supprimer();
				this.prets.remove(m);
				return m.getPOJO();
			}
		}
		throw new NotFoundException();
	}
}
