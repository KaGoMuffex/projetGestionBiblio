package com.ynov.service;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name ="livre")
@XmlRootElement(name="livre")
public class LivrePOJO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LivrePOJO() {
		super();
	}
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String titre;
	private String resume;
	private int quantite;
	private String isbn;
	private String urlPhoto;
	@Temporal(TemporalType.DATE)
	private Date datePublication;
	private AuteurPOJO auteur;
	private CategoriePOJO categorie;

	public LivrePOJO(Long id, String titre, String resume, int quantite, String isbn, String urlPhoto, Date datePublication,
			AuteurPOJO auteur, CategoriePOJO categorie) {
		super();
		this.id = id;
		this.titre = titre;
		this.resume = resume;
		this.quantite = quantite;
		this.isbn = isbn;
		this.urlPhoto = urlPhoto;
		this.datePublication = datePublication;
		this.auteur = auteur;
		this.categorie = categorie;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getResume() {
		return resume;
	}

	public void setResume(String resume) {
		this.resume = resume;
	}

	public int getQuantite() {
		return quantite;
	}

	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getUrlPhoto() {
		return urlPhoto;
	}

	public void setUrlPhoto(String urlPhoto) {
		this.urlPhoto = urlPhoto;
	}

	public Date getDatePublication() {
		return datePublication;
	}

	public void setDatePublication(Date datePublication) {
		this.datePublication = datePublication;
	}

	public AuteurPOJO getAuteur() {
		return auteur;
	}

	public void setAuteur(AuteurPOJO auteur) {
		this.auteur = auteur;
	}

	public CategoriePOJO getCategorie() {
		return categorie;
	}

	public void setCategorie(CategoriePOJO categorie) {
		this.categorie = categorie;
	}

}
