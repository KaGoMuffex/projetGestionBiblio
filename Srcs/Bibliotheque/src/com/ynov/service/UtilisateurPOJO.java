package com.ynov.service;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import com.ynov.utilitaire.Role;
import com.ynov.utilitaire.StatutCompte;

@Entity
@Table(name ="utilisateur")
@XmlRootElement(name="utilisateur")
public class UtilisateurPOJO extends PersonnePOJO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String pseudo;
	private String mdp;
	private StatutCompte statutCompte;
	@Temporal(TemporalType.DATE)
	private Date derniereConnexion;
	private Role role;

	public UtilisateurPOJO() {
		// TODO Auto-generated constructor stub
	}
	
	public UtilisateurPOJO(Long id, String nom, String prenom, String sexe, String urlPhoto, String email,
			Date dateDeNaissance, String pseudo, String mdp, StatutCompte statutCompte, Date derniereConnexion,
			Role role) {
		super(id, nom, prenom, sexe, urlPhoto, email, dateDeNaissance);
		this.pseudo = pseudo;
		this.mdp = mdp;
		this.statutCompte = statutCompte;
		this.derniereConnexion = derniereConnexion;
		this.role = role;
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public String getMdp() {
		return mdp;
	}

	public void setMdp(String mdp) {
		this.mdp = mdp;
	}

	public StatutCompte getStatutCompte() {
		return statutCompte;
	}

	public void setStatutCompte(StatutCompte statutCompte) {
		this.statutCompte = statutCompte;
	}

	public Date getDerniereConnexion() {
		return derniereConnexion;
	}

	public void setDerniereConnexion(Date derniereConnexion) {
		this.derniereConnexion = derniereConnexion;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
}
