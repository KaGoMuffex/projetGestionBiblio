package com.ynov.service.web;

import java.util.List;

import javax.jws.HandlerChain;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.servlet.UnavailableException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ynov.controleur.Manager;
import com.ynov.model.technique.DuplicatedEmailException;
import com.ynov.model.technique.DuplicatedPseudoException;
import com.ynov.model.technique.ForbiddenException;
import com.ynov.service.AuteurPOJO;
import com.ynov.service.CategoriePOJO;
import com.ynov.service.LivrePOJO;
import com.ynov.service.PretPOJO;
import com.ynov.service.UtilisateurPOJO;

@Path(".")
public class RestfullService {
	
	public RestfullService() {
	}
	
	// reconnecter l'utilisateur au manager
	public void userConnexion(HttpSession session) {
		UtilisateurPOJO user =  (UtilisateurPOJO) session.getAttribute("user");
		if(user != null) {
			Manager.getInstance().connexion(user.getPseudo(), user.getMdp(), false);
		}
	}
	
	/// Utilisateur actuel

	@PUT
	@Path("/connexion")
	public void connexion(@Context HttpServletRequest req,  @FormParam(value="pseudo")String pseudo, @FormParam(value="mdp")String mdp) {
		if(Manager.getInstance().connexion(pseudo, mdp)) {
			req.getSession().setAttribute("user", Manager.getInstance().getUtilisateurConnecte());
		} else {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
	}
	


	@GET
	@Path("/deconnexion")
	public void deconnexion(@Context HttpServletRequest req) {
		Manager.getInstance().deconnexion();
		req.getSession().removeAttribute("user");
	}
	
	/// AUTEUR

	@GET
	@Path("/auteurs")
	@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
	public List<AuteurPOJO> consultationAuteurs(@Context HttpServletRequest req) {
		userConnexion(req.getSession());
		return Manager.getInstance().consultationAuteurs();
	}
	
	@GET
	@Path("/auteur/{idAuteur}")
	@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
	public AuteurPOJO consultationAuteur(@Context HttpServletRequest req, @PathParam(value="idAuteur") Long id) {
		userConnexion(req.getSession());
		return Manager.getInstance().consultationAuteur(id);
	}
	
	@PUT
	@Path("/auteur/{id}")
	@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
	public void modifierAuteur(@Context HttpServletRequest req, @PathParam(value="id") Long id, AuteurPOJO newAuteur) {
		userConnexion(req.getSession());
		newAuteur.setId(id);
		try {
			Manager.getInstance().modifierAuteur(newAuteur.getId(), newAuteur);
		} catch (ForbiddenException e) {
			throw new WebApplicationException(Response.Status.FORBIDDEN);
		}
	}
	
	@POST
	@Path("/auteur")
	@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
	public void ajouterAuteur(@Context HttpServletRequest req, AuteurPOJO newAuteur) {
		userConnexion(req.getSession());
		try {
			Manager.getInstance().ajouterAuteur(newAuteur);
		} catch (ForbiddenException e) {
			throw new WebApplicationException(Response.Status.FORBIDDEN);
		}
	}

	@DELETE
	@Path("/auteur")
	@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
	public AuteurPOJO supprimerAuteur(@Context HttpServletRequest req, @FormParam(value="idAuteur")Long id) {
		System.out.println(id);
		userConnexion(req.getSession());
		try {
			return Manager.getInstance().supprimerAuteur(id);
		} catch (ForbiddenException e) {
			throw new WebApplicationException(Response.Status.FORBIDDEN);
		}
	}
	
	/// UTILISATEUR

		@GET
		@Path("/utilisateurs")
		@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
		public List<UtilisateurPOJO> consultationUtilisateurs(@Context HttpServletRequest req) {
			userConnexion(req.getSession());
			
			try {
				return Manager.getInstance().consultationUtilisateurs();
			} catch (ForbiddenException e) {
				throw new WebApplicationException(Response.Status.FORBIDDEN);
			}
		}
		
		@GET
		@Path("/utilisateur/{idUtilisateur}")
		@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
		public UtilisateurPOJO consultationUtilisateur(@Context HttpServletRequest req, @PathParam(value="idUtilisateur") Long id) {
			userConnexion(req.getSession());
			try {
				return Manager.getInstance().consultationUtilisateur(id);
			} catch (ForbiddenException e) {
				throw new WebApplicationException(Response.Status.FORBIDDEN);
			}
		}
		
		@PUT
		@Path("/utilisateur/{id}")
		@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
		public void modifierUtilisateur(@Context HttpServletRequest req, @PathParam(value="id") Long id, UtilisateurPOJO newUtilisateur) {
			userConnexion(req.getSession());
			newUtilisateur.setId(id);
			try {
				Manager.getInstance().modifierUtilisateur(newUtilisateur.getId(), newUtilisateur);
				req.getSession().setAttribute("user", newUtilisateur);
			} catch (ForbiddenException e) {
				throw new WebApplicationException(Response.Status.FORBIDDEN);
			}
		}
		
		@POST
		@Path("/utilisateur")
		@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
		public void ajouterUtilisateur(@Context HttpServletRequest req, UtilisateurPOJO newUtilisateur) {
			userConnexion(req.getSession());
			try {
				Manager.getInstance().ajouterUtilisateur(newUtilisateur);
			} catch (DuplicatedEmailException e) {
				throw new WebApplicationException(Response.Status.CONFLICT);
			} catch (DuplicatedPseudoException e) {
				throw new WebApplicationException(Response.Status.CONFLICT);
			}
		}

		@DELETE
		@Path("/utilisateur")
		@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
		public UtilisateurPOJO supprimerUtilisateur(@Context HttpServletRequest req, @FormParam(value="idUtilisateur")Long id) {
			userConnexion(req.getSession());
			try {
				return Manager.getInstance().supprimerUtilisateur(id);
			} catch (ForbiddenException e) {
				throw new WebApplicationException(Response.Status.FORBIDDEN);
			}
		}
		
		/// LIVRE

		@GET
		@Path("/livres")
		@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
		@WebMethod
		public List<LivrePOJO> consultationLivres(@Context HttpServletRequest req) {
			userConnexion(req.getSession());
			return Manager.getInstance().consultationLivres();
		}
		
		@GET
		@Path("/livre/{idLivre}")
		@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
		public LivrePOJO consultationLivre(@Context HttpServletRequest req, @PathParam(value="idLivre") Long id) {
			userConnexion(req.getSession());
			return Manager.getInstance().consultationLivre(id);
		}
		
		@PUT
		@Path("/livre/{id}")
		@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
		public void modifierLivre(@Context HttpServletRequest req, @PathParam(value="id") Long id, LivrePOJO newLivre) {
			userConnexion(req.getSession());
			newLivre.setId(id);
			try {
				Manager.getInstance().modifierLivre(newLivre.getId(), newLivre);
			} catch (ForbiddenException e) {
				throw new WebApplicationException(Response.Status.FORBIDDEN);
			}
		}
		
		@POST
		@Path("/livre")
		@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
		public void ajouterLivre(@Context HttpServletRequest req, LivrePOJO newLivre) {
			userConnexion(req.getSession());
			try {
				Manager.getInstance().ajouterLivre(newLivre);
			} catch (ForbiddenException e) {
				throw new WebApplicationException(Response.Status.FORBIDDEN);
			}
		}

		@DELETE
		@Path("/livre")
		@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
		public LivrePOJO supprimerLivre(@Context HttpServletRequest req, @FormParam(value="idLivre")Long id) {
			System.out.println(id);
			userConnexion(req.getSession());
			try {
				return Manager.getInstance().supprimerLivre(id);
			} catch (ForbiddenException e) {
				throw new WebApplicationException(Response.Status.FORBIDDEN);
			}
		}
		
		@PUT
		@Path("/emprunter/{idLivre}")
		@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
		public void emprunterLivre(@Context HttpServletRequest req, @PathParam(value="idLivre") Long id) {
			userConnexion(req.getSession());
			try {
				Manager.getInstance().emprunterLivre(id);
			} catch (ForbiddenException e) {
				throw new WebApplicationException(Response.Status.FORBIDDEN);
			} catch (UnavailableException e) {
				throw new WebApplicationException(Response.Status.SERVICE_UNAVAILABLE);
			}
		}
		
		/// PRET

		@GET
		@Path("/prets")
		@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
		public List<PretPOJO> consultationPrets(@Context HttpServletRequest req) {
			userConnexion(req.getSession());
			try {
				return Manager.getInstance().consultationPrets();
			} catch (ForbiddenException e) {
				throw new WebApplicationException(Response.Status.FORBIDDEN);
			}
		}
		
		@GET
		@Path("/pret/{idPret}")
		@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
		public PretPOJO consultationPret(@Context HttpServletRequest req, @PathParam(value="idPret") Long id) {
			userConnexion(req.getSession());
			try {
				return Manager.getInstance().consultationPret(id);
			} catch (ForbiddenException e) {
				throw new WebApplicationException(Response.Status.FORBIDDEN);
			}
		}
		
		@PUT
		@Path("/pret/{id}")
		@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
		public void modifierPret(@Context HttpServletRequest req, @PathParam(value="id") Long id, PretPOJO newPret) {
			userConnexion(req.getSession());
			newPret.setId(id);
			try {
				Manager.getInstance().modifierPret(newPret.getId(), newPret);
			} catch (ForbiddenException e) {
				throw new WebApplicationException(Response.Status.FORBIDDEN);
			}
		}
		
		@POST
		@Path("/pret")
		@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
		public void ajouterPret(@Context HttpServletRequest req, PretPOJO newPret) {
			userConnexion(req.getSession());
			try {
				Manager.getInstance().ajouterPret(newPret);
			} catch (ForbiddenException e) {
				throw new WebApplicationException(Response.Status.FORBIDDEN);
			}
		}

		@DELETE
		@Path("/pret")
		@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
		public PretPOJO supprimerPret(@Context HttpServletRequest req, @FormParam(value="idPret")Long id) {
			userConnexion(req.getSession());
			try {
				return Manager.getInstance().supprimerPret(id);
			} catch (ForbiddenException e) {
				throw new WebApplicationException(Response.Status.FORBIDDEN);
			}
		}
		
		/// CATEGORIE

		@GET
		@Path("/categories")
		@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
		public List<CategoriePOJO> consultationCategories(@Context HttpServletRequest req) {
			userConnexion(req.getSession());
			return Manager.getInstance().consultationCategories();
		}
		
		@GET
		@Path("/categorie/{idCategorie}")
		@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
		public CategoriePOJO consultationCategorie(@Context HttpServletRequest req, @PathParam(value="idCategorie") Long id) {
			userConnexion(req.getSession());
			return Manager.getInstance().consultationCategorie(id);
		}
		
		@PUT
		@Path("/categorie/{id}")
		@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
		public void modifierCategorie(@Context HttpServletRequest req, @PathParam(value="id") Long id, CategoriePOJO newCategorie) {
			userConnexion(req.getSession());
			newCategorie.setId(id);
			try {
				Manager.getInstance().modifierCategorie(newCategorie.getId(), newCategorie);
			} catch (ForbiddenException e) {
				throw new WebApplicationException(Response.Status.FORBIDDEN);
			}
		}
		
		@POST
		@Path("/categorie")
		@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
		public void ajouterCategorie(@Context HttpServletRequest req, CategoriePOJO newCategorie) {
			userConnexion(req.getSession());
			try {
				Manager.getInstance().ajouterCategorie(newCategorie);
			} catch (ForbiddenException e) {
				throw new WebApplicationException(Response.Status.FORBIDDEN);
			}
		}

		@DELETE
		@Path("/categorie")
		@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
		public CategoriePOJO supprimerCategorie(@Context HttpServletRequest req, @FormParam(value="idCategorie")Long id) {
			userConnexion(req.getSession());
			try {
				return Manager.getInstance().supprimerCategorie(id);
			} catch (ForbiddenException e) {
				throw new WebApplicationException(Response.Status.FORBIDDEN);
			}
		}

}
