package com.ynov.utilitaire;

import com.ynov.model.metier.Auteur;
import com.ynov.model.metier.Categorie;
import com.ynov.model.metier.Livre;
import com.ynov.model.metier.Pret;
import com.ynov.model.metier.Utilisateur;
import com.ynov.service.AuteurPOJO;
import com.ynov.service.CategoriePOJO;
import com.ynov.service.LivrePOJO;
import com.ynov.service.PretPOJO;
import com.ynov.service.UtilisateurPOJO;

public class Conversion {
    public static Auteur pojoToAuteur(AuteurPOJO pojo) {
        return new Auteur(pojo.getId(), pojo.getNom(), pojo.getPrenom(), pojo.getSexe(), pojo.getUrlPhoto(), pojo.getEmail(), pojo.getDateDeNaissance(), pojo.getaProposDe(), pojo.getNationalite(), pojo.getType());
    }

    public static Utilisateur pojoToUtilisateur(UtilisateurPOJO pojo) {        
        return new Utilisateur(pojo.getId(), pojo.getNom(), pojo.getPrenom(), pojo.getSexe(), pojo.getUrlPhoto(), pojo.getEmail(), pojo.getDateDeNaissance(), pojo.getPseudo(), pojo.getMdp(), pojo.getStatutCompte(), pojo.getDerniereConnexion(), pojo.getRole());
    }

    public static Categorie pojoToCategorie(CategoriePOJO pojo) {        
        return new Categorie(pojo.getId(), pojo.getNom(), pojo.getDecription());
    }

    public static Livre pojoToLivre(LivrePOJO pojo) {        
        return new Livre(pojo.getId(), pojo.getTitre(), pojo.getResume(), pojo.getQuantite(), pojo.getIsbn(), pojo.getUrlPhoto(), pojo.getDatePublication(), Conversion.pojoToAuteur(pojo.getAuteur()), Conversion.pojoToCategorie(pojo.getCategorie()));
    }

    public static Livre pojoToLivre(LivrePOJO pojo, Auteur auteur, Categorie categorie) {        
        return new Livre(pojo.getId(), pojo.getTitre(), pojo.getResume(), pojo.getQuantite(), pojo.getIsbn(), pojo.getUrlPhoto(), pojo.getDatePublication(), auteur, categorie);
    }

    public static Pret pojoToPret(PretPOJO pojo) {        
        return new Pret(pojo.getId(), Conversion.pojoToLivre(pojo.getLivre()), Conversion.pojoToUtilisateur(pojo.getUtilisateur()), pojo.getDateDePret(), pojo.getDuree());
    }

    public static Pret pojoToPret(PretPOJO pojo, Livre livre, Utilisateur utilisateur) {        
        return new Pret(pojo.getId(), livre, utilisateur, pojo.getDateDePret(), pojo.getDuree());
    }
    
}
