package com.ynov.model.metier;

import java.util.Date;

import com.ynov.utilitaire.Role;
import com.ynov.utilitaire.StatutCompte;

public class Utilisateur extends Personne {

	private String pseudo;
	private String mdp;
	private StatutCompte statutCompte;
	private Date derniereConnexion;
	private Role role;
	
	public Utilisateur(Long id, String nom, String prenom, String sexe, String urlPhoto, String email,
			Date dateDeNaissance, String pseudo, String mdp, StatutCompte statutCompte, Date derniereConnexion,
			Role role) {
		super(id, nom, prenom, sexe, urlPhoto, email, dateDeNaissance);
		this.pseudo = pseudo;
		this.mdp = mdp;
		this.statutCompte = statutCompte;
		this.derniereConnexion = derniereConnexion;
		this.role = role;
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public String getMdp() {
		return mdp;
	}

	public void setMdp(String mdp) {
		this.mdp = mdp;
	}

	public StatutCompte getStatutCompte() {
		return statutCompte;
	}

	public void setStatutCompte(StatutCompte statutCompte) {
		this.statutCompte = statutCompte;
	}

	public Date getDerniereConnexion() {
		return derniereConnexion;
	}

	public void setDerniereConnexion(Date derniereConnexion) {
		this.derniereConnexion = derniereConnexion;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
	
	public boolean connexion(String pseudo, String mdp) {
		// TODO v�rifier dans la base de donn�e
		return false;
	}

	public boolean emprunter(Long idLivre) {
		// TODO v�rifier si l'utilisateur peut emprunter le livre
		return false;
	}

}
