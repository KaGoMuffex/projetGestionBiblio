package com.ynov.model.technique;

public class DuplicatedPseudoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DuplicatedPseudoException() {
		// TODO Auto-generated constructor stub
		super("Le pseudo sp�cifi� existe d�j�");
	}

	public DuplicatedPseudoException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public DuplicatedPseudoException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public DuplicatedPseudoException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public DuplicatedPseudoException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}
}
