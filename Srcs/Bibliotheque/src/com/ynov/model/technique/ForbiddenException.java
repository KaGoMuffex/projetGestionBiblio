package com.ynov.model.technique;

public class ForbiddenException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ForbiddenException() {
		// TODO Auto-generated constructor stub
		super("Les droits d'acc�s ne permettent pas au client d'acc�der � la ressource");
	}

	public ForbiddenException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ForbiddenException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public ForbiddenException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ForbiddenException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
