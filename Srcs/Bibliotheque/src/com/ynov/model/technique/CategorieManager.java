package com.ynov.model.technique;

import com.ynov.model.metier.Categorie;
import com.ynov.service.CategoriePOJO;
import com.ynov.stockage.Dao;
import com.ynov.utilitaire.Conversion;

public class CategorieManager {
	private Categorie categorie;
	private CategoriePOJO sp;
	private Dao<CategoriePOJO> dao;
	private Long cle;

	/**
	 * Constructeur par defaut qui init une categorie manager
	 */
	public CategorieManager() {
		cle = -1L;
		categorie = null;
		sp = null;
		dao = null;
	}

	/**
	 * Constructeur parametrer o� l'on choisi l'index et la dao utiliser
	 * @param clef
	 * @param d
	 */
	public CategorieManager(Long clef, Dao<CategoriePOJO> d) {
		cle = clef;
		dao = d;
		init();
	}

	/**
	 * Constructeur parametrer o� l'on choisi la categoriepojo et la dao utiliser
	 * @param p
	 * @param d
	 */
	public CategorieManager(CategoriePOJO p, Dao<CategoriePOJO> d) {
		cle = p.getId();
		sp = p;
		dao = d;
		categorie = Conversion.pojoToCategorie(p);
	}

	/**
	 * cette methode va permettre d'initialiser une categorie � partir de sa categoriepojo grace a le methode de conversion
	 * @see Conversion
	 */
	private void init() {
		sp = dao.lire(cle);
		categorie = Conversion.pojoToCategorie(sp);
	}

	/**
	 * retourne les infos de la categorie manager
	 */
	@Override
	public String toString() {
		return String.join(" ", "id:", ("" + cle), categorie.toString());
	}

	/**
	 * 
	 * @return la categorie pojo
	 */
	public CategoriePOJO getPOJO() {
		return sp;
	}

	
	/**
	 * Cette methode va permettre de supprimer une categorie de la bdd
	 */
	public void supprimer() {
		if(categorie != null){
			dao.effacer(cle);
			categorie = null;
		}
	}

	/**
	 * Cette methode va ajouter une categorie � la bdd
	 * @param sp
	 */
	public void ajouter(CategoriePOJO sp) {
		dao.inserer(sp);
	}

	/**
	 * Cette methode va metre a jour l'utilisateur � la bdd
	 * @param sp
	 */
	public void update(CategoriePOJO sp) {
		if(categorie != null) {
			dao.update(categorie.getId(), sp);
			this.sp = sp;
		}
	}
}
