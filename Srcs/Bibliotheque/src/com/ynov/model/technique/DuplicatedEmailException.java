package com.ynov.model.technique;

public class DuplicatedEmailException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DuplicatedEmailException() {
		// TODO Auto-generated constructor stub
		super("L'email sp�cifi� existe d�j�");
	}

	public DuplicatedEmailException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public DuplicatedEmailException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public DuplicatedEmailException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public DuplicatedEmailException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}
}
