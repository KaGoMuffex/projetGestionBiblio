package com.ynov.stockage;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

import com.ynov.service.LivrePOJO;

public class DaoLivre implements Dao<LivrePOJO> {

	@PersistenceContext(unitName="Bibliotheque")
	private EntityManager em;
	
	public DaoLivre() {
		//construire le CONTEXTE JPA
		em = Persistence.createEntityManagerFactory("Bibliotheque").createEntityManager();
	}
	
	@Override
	public List<LivrePOJO> lireTous() {
		return em.createQuery("SELECT l FROM LivrePOJO l",LivrePOJO.class).getResultList();
	}

	@Override
	public LivrePOJO lire(Long cle) {
		return em.find(LivrePOJO.class, cle);
	}

	@Override
	public void inserer(LivrePOJO elt) {
		em.getTransaction().begin();
		em.persist(elt);
		em.getTransaction().commit();
	}

	@Override
	public void update(Long index, LivrePOJO obj) {
		update(obj);
	}
	
	public void update(LivrePOJO obj) {
		em.getTransaction().begin();
		em.merge((LivrePOJO)obj);
		em.getTransaction().commit();
	}

	@Override
	public void effacer(Long cle) {
		LivrePOJO tmp = null;
		em.getTransaction().begin();
		tmp = em.find(LivrePOJO.class, cle);
		em.remove(tmp);
		em.getTransaction().commit();
	}

}
