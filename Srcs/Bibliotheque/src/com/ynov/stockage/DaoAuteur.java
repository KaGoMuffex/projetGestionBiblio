package com.ynov.stockage;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

import com.ynov.service.AuteurPOJO;

public class DaoAuteur implements Dao<AuteurPOJO> {
	@PersistenceContext(unitName="Bibliotheque")
	private EntityManager em;
	
	public DaoAuteur() {
		//construire le CONTEXTE JPA
		em = Persistence.createEntityManagerFactory("Bibliotheque").createEntityManager();
	}
	
	@Override
	public List<AuteurPOJO> lireTous() {
		return em.createQuery("SELECT a FROM AuteurPOJO a",AuteurPOJO.class).getResultList();
	}

	@Override
	public AuteurPOJO lire(Long cle) {
		return em.find(AuteurPOJO.class, cle);
	}

	@Override
	public void inserer(AuteurPOJO elt) {
		em.getTransaction().begin();
		em.persist(elt);
		em.getTransaction().commit();
	}

	@Override
	public void update(Long index, AuteurPOJO obj) {
		update(obj);
	}
	
	public void update(AuteurPOJO obj) {
		em.getTransaction().begin();
		em.merge((AuteurPOJO)obj);
		em.getTransaction().commit();
	}

	@Override
	public void effacer(Long cle) {
		AuteurPOJO tmp = null;
		em.getTransaction().begin();
		tmp = em.find(AuteurPOJO.class, cle);
		em.remove(tmp);
		em.getTransaction().commit();
	}

}
