package com.ynov.stockage;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

import com.ynov.service.UtilisateurPOJO;

public class DaoUtilisateur implements Dao<UtilisateurPOJO> {

	@PersistenceContext(unitName="Bibliotheque")
	private EntityManager em;
	
	public DaoUtilisateur() {
		//construire le CONTEXTE JPA
		em = Persistence.createEntityManagerFactory("Bibliotheque").createEntityManager();
	}
	
	@Override
	public List<UtilisateurPOJO> lireTous() {
		return em.createQuery("SELECT u FROM UtilisateurPOJO u",UtilisateurPOJO.class).getResultList();
	}

	@Override
	public UtilisateurPOJO lire(Long cle) {
		return em.find(UtilisateurPOJO.class, cle);
	}

	@Override
	public void inserer(UtilisateurPOJO elt) {
		em.getTransaction().begin();
		em.persist(elt);
		em.getTransaction().commit();
	}

	@Override
	public void update(Long index, UtilisateurPOJO obj) {
		update(obj);
	}
	
	public void update(UtilisateurPOJO obj) {
		em.getTransaction().begin();
		em.merge((UtilisateurPOJO)obj);
		em.getTransaction().commit();
	}

	@Override
	public void effacer(Long cle) {
		UtilisateurPOJO tmp = null;
		em.getTransaction().begin();
		tmp = em.find(UtilisateurPOJO.class, cle);
		em.remove(tmp);
		em.getTransaction().commit();
	}

}
